import {Component, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CSV Upload';
  file: File;
  data: any[];
  dataFromExcel: any[];
  count: number;
  errorMessage: string;
  tableData: MatTableDataSource<any>;
  displayedColumns: string[] = ['Camper_Make', 'Camper_Brand', 'Sleep_Number', 'Price'];
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor() {
    this.data = [];
    this.dataFromExcel = [];
    this.count = 0;
    this.errorMessage = '';
  }

  incomingFile(event) {
    this.file = event.target.files[0];
  }
  upload() {
    this.errorMessage = '';
    const fileReader: FileReader = new FileReader();
    fileReader.readAsText(this.file);
    fileReader.onload = (e) => {
      const csv: string = fileReader.result as string;
      this.data = this.csvJSON(csv);
      this.tableData = new MatTableDataSource(this.data);
      this.tableData.sort = this.sort;
      console.log(this.data);
    };
  }

  public csvJSON(csv) {
    const lines = csv.replace('\n', '').split('\r');
    const result = [];
    const headers: string[] = lines[0].split(',');
    for (let x = 0; x < headers.length; x++) {
      const currentHeader = headers[x].trim().replace(' ', '_');
      headers[x] = currentHeader;
    }
    for (let i = 1; i < lines.length; i++) {
      const obj: any = {};
      const currentline: string[] = lines[i].split(',');
      for (let j = 0; j < headers.length; j++) {
        obj[headers[j]] = currentline[j] ? currentline[j].trim().replace('/n', '') : null;
      }
      if (obj.Camper_Make != null && obj.Camper_Make.length > 0) {
        result.push(obj);
      }
    }
    return result;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableData.filter = filterValue.trim().toLowerCase();
  }
}
